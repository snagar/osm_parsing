#!/usr/bin/perl
use 5.010;
use DBI;
use strict;
use warnings;

use XML::LibXML;
use XML::LibXML::Reader;
use Time::HiRes;

#### Timelapse ######
my $start_time = Time::HiRes::gettimeofday();

#### Global Variables
my $seq = 0;
my $lines=0;
my $nodes_conter=0;
my $way_counter=0;
my $ref_search_counter=0; # overall <nodes> we searched until we found a match
my $match_ref_counter=0;
my $prev_node_type ="";

# my $overall_ref_node_counter=0; # overall <nodes> we searched until we found a match

## https://www.tutorialspoint.com/sqlite/sqlite_perl.htm
my $driver   = "SQLite"; 
my $database = "osmdb.sqlite.db";
my $dsn = "DBI:$driver:dbname=$database";
my $userid = "";
my $password = "";
my $dbh = DBI->connect($dsn, $userid, $password, { RaiseError => 1 }) 
   or die $DBI::errstr;

print "Opened database successfully\n";


## https://grantm.github.io/perl-libxml-by-example/dom.html

### open XML file 
my %arg_map;
my $filename = ""; #$ARGV[0];
my $task = ""; # node or way or all
my $rebuild_node_data = ""; # if yes then we will 
my $rebuild_node_tag_data = ""; # if yes then we will 
my $rebuild_ways = ""; # rebuild all ways information + tables
# print @ARGV; #debug

# exit 0; # debug

foreach my $ext_arg (@ARGV)
{
  my @words = split ('=', $ext_arg);
  if ( scalar  @words >= 2 )
  {
    $arg_map{$words[0]} = $words[1];
  }
  else 
  {
    $arg_map{$words[0]} = $words[0];    
  }
}

### Assignments of flags

if ( exists ($arg_map{"help"}))
{
 print "\nThis script was written to convert an OSM <node> and <way> elements into tables and rows of information.\n";
 print "This should allow for faster search of data.";
 print "It is strongly adviced to prepare an OSM of specific area or reagion you would like to query, and not a whole continent.\n\n";
 print "Syntax:\n=======\n{script name} *file={filename} *task=[all|node|way] rebuild_node_data=[yes|no] rebuild_node_tag_data=[yes|no] rebuild_ways=[yes|no]\n\n";
 print "Example: \n";
 print "\t* = mandatory \n";
 print "\t{script name} file=alaska_filtered.osm task=all\t\tWill parse and rebuild all <node> and <way> tags in file. This includes table rebuild. \n";
 print "\t{script name} file=alaska_filtered.osm task=node\tWill parse and rebuild only <node> tags in file. It won't rebuild node tables\n";
 print "\t{script name} file=alaska_filtered.osm task=node rebuild_node_data=yes\t\tWill parse and rebuild only <node> tags in file. It will rebuild node tables but won't process the <node> tags into 'node_tag_data' table\n";
 print "";
 print "";
 print "";
 
 exit 0;
} 


if ( exists ( $arg_map{"file"} ) )
{
  $filename = $arg_map{"file"};
} 
else{
  die "No input file was defined.";
} 

if ( exists ($arg_map{"task"}))
{
  $task = $arg_map{"task"};
} else { 
  die "No task was defined (node or way or all"; 
}


if ( exists ($arg_map{"rebuild_node_data"}))
{
  $rebuild_node_data = $arg_map{"rebuild_node_data"};
} 

if ( exists ($arg_map{"rebuild_node_tag_data"}))
{
  $rebuild_node_tag_data = $arg_map{"rebuild_node_tag_data"};
} 

if ( exists ($arg_map{"rebuild_ways"}))
{
  $rebuild_ways = $arg_map{"rebuild_ways"};
} 


rebuild_tables();

##### Prepare ahead of time so we won't need to do this every ref iteration
my $stmt_fetch_node_by_id = qq(SELECT id, lat, lon FROM node_data where id = ? LIMIT 1;); # fetch only 1 row
my $sth_fetch_node_by_id = $dbh->prepare( $stmt_fetch_node_by_id );


## repetitive function to execute in the database
sub exec_stmt
{
  my $xxstmt = $_[0];
  my $xxsuccess_msg = $_[1];
  
  $xxstmt =~ s/^\s+|\s+$//g;
  print "$xxstmt\n";


  my $rv = $dbh->do("$xxstmt");
  if($rv < 0) {
    print $DBI::errstr;
  } 
  
  if ($xxsuccess_msg ne "")
  {
    print $xxsuccess_msg;
  }

}

# rebuild tables in database before the load
sub rebuild_tables
{
  my $stmt="";
  # # # my $stmt = qq( drop table if exists all_node_data; ); ## Drop ALL NODES huge table

  # # # exec_stmt ($stmt, "dropped table all_node_data.");

  if ( $rebuild_node_data eq "yes" || $task eq "all" ) ## Drop table only if we asked in the command line
  {
    print "Dropping <node> based tables.\n";

    $stmt = qq( drop table if exists node_tag_data; );

    exec_stmt ($stmt, "dropped table node_tag_data.");

    $stmt = qq( drop table if exists node_data; );

    exec_stmt ($stmt, "dropped table node_data.");
  }

  if ($rebuild_ways eq "yes" || $task eq "all" )
  {
    print "Dropping <way> based tables.\n";
    $stmt = qq( drop table if exists way_data; );

    exec_stmt ($stmt, "dropped table way_data.");

    $stmt = qq( drop table if exists way_tag_data; );

    exec_stmt ($stmt, "dropped table way_tag_data.");

    $stmt = qq( drop table if exists way_street_node_data; );

    exec_stmt ($stmt, "dropped table way_street_node_data.");
  }



# exit 0;
### node_data
  # $stmt = qq(CREATE TABLE IF NOT EXISTS all_node_data
  #   (
  #     id INTEGER PRIMARY KEY,
  #     lat REAL NULL,
  #     lon REAL NULL
  #   );
  # ); # end qq. Create node_data table

  # exec_stmt ($stmt, "created table all_node_data.");

  $stmt = qq(CREATE TABLE IF NOT EXISTS node_data
    (
      id INTEGER PRIMARY KEY,
      -- tag_type CHARACTER(1) NOT NULL,
      lat REAL NULL,
      lon REAL NULL,
      name TEXT NULL
    );
  ); # end qq. Create node_data table

  exec_stmt ($stmt, "created table node_data.");

  $stmt = qq(CREATE TABLE IF NOT EXISTS node_tag_data
    (
      id integer NOT NULL,
      key_attrib text NOT NULL,
      val_attrib text NULL
    );
  ); # end qq. Create tag_data table

exec_stmt ($stmt, "created table node_tag_data.");

### way_data
  $stmt = qq(CREATE TABLE IF NOT EXISTS way_data
    (
      id INTEGER PRIMARY KEY,
      -- tag_type CHARACTER(1) NULL,
      -- lat REAL NULL,
      -- lon REAL NULL,
      text TEXT NULL
    );
  ); # end qq. Create way_data table

  exec_stmt ($stmt, "created table way_data.");

  $stmt = qq(CREATE TABLE IF NOT EXISTS way_tag_data
    (
      id integer NOT NULL,
      key_attrib text NOT NULL,
      val_attrib text NULL
    );
  ); # end qq. Create tag_data table

  exec_stmt ($stmt, "created table way_tag_data.");

  $stmt = qq(CREATE TABLE IF NOT EXISTS way_street_node_data
    (
      id NOT NULL,
      node_id INTEGER NOT NULL,
      lat REAL NOT NULL,
      lon REAL NOT NULL
    );
  ); # end qq. Create way_data table

  exec_stmt ($stmt, "way_street_node_data");




# ### ref_data
#   $stmt = qq(CREATE TABLE rel_data
#     (
#       id INTEGER PRIMARY KEY,
#       tag_type CHARACTER(1) NOT NULL,
#       lat REAL NULL,
#       lon REAL NULL,
#       name TEXT NULL
#     );
#   ); # end qq. Create rel_data table

#   exec_stmt ($stmt, "created table rel_data.");

#   $stmt = qq(CREATE TABLE rel_tag_data
#     (
#       id integer NOT NULL,
#       tag charachter(1) NOT NULL, 
#       key_attrib text NOT NULL,
#       val_attrib text NULL
#     );
#   ); # end qq. Create rel_tag_data table

# exec_stmt ($stmt, "created table rel_tag_data.");


} # end rebuild_tables



rebuild_tables();
print "After rebuild_tables. \n";
# exit 0;



#print %arg_map; #debug

# exit 0;

# my $str = "-145.6082547,62.8518168,-145.5035520,62.9872186";
# # #             my @words = split /,/, $v;
# # #             # my %bbox_pos;

# # #             if ( scalar @words >=4 )
# # #             {
# # #               # print "we have enough values\n";
# # #               # $bbox_pos{"lon1"} = $words[0];
# # #               # $bbox_pos{"lat1"} = $words[1];
# # #               # $bbox_pos{"lon2"} = $words[2];
# # #               # $bbox_pos{"lat2"} = $words[3];


my $tag_name_to_parse = "node"; # $ARGV[1]; ## to parse only one element instead the whole file. If empty then whole file will be parsed.

# DOM
# my $dom = XML::LibXML->load_xml(location => $filename)
#   or die "Fail to open XML file";

# READER
# my $reader = XML::LibXML::Reader->new(location => $filename, '<:encoding(ASCII)')  # auto decoding on read
# my $reader = XML::LibXML::Reader->new(location => $filename)  # auto decoding on read
#        or die "cannot read file.xml\n";  
  


## XPATH filterring
my $my_xpath = q{./tag[contains(@k,'name')]/..}; # default for <node> and <way>
my $tags_xpath = q{./tag};
# my @attr = ( "name", "name:en", "place", "description", "natural", "amenity", "is_in" );
# my %attrib_list_to_store = ('name', 'name', 'name:en', 'name:en', 'place', 'place', 'natural', 'natural', 'amenity', 'amenity', 'is_in', 'is_in', 'description', 'description', "bBox", "bBox", "bBoxArea", "bBoxArea", "type", "type", "water", "water", "waterway", "waterway" );


sub parse_way_nodes
{
  my $dom = $_[0];
  my $stmt_tag = "";
  my $my_xpath = q{./tag[contains(@k,'name')]/..}; # work on nodes that have name attrib in their "<tag> sub element".
  
  $seq=$seq+1;

  #print ("testing way:\n$dom\n"); ## debug
  if(my($node) = $dom->findnodes($my_xpath)) 
  { 
    $way_counter=$way_counter+1;
    
    
    my $parent_node_type    = substr ($node->nodeName, 0, 1)  ; # retrieve current node name
    my $way_id  = $node->findnodes('./@id'); # latitude    
    
    my $table = "way_data";
    my $tag_table = "way_tag_data";
    $tags_xpath = q{./*}; # read all sub elements since we might have <nd> tage for street vectors
    

    # store way header information
    my $stmt_main = qq( insert into ${table} ( id ) values ( ? ); ); # end qq. Create tag_data table                 
    my $rv = $dbh->do ($stmt_main, undef, $way_id);            
    if($rv < 0) {
      print $DBI::errstr;
      print "$parent_node_type id: $way_id \n";
    }     


    ## Read all sub nodes 
    foreach my $tag ($node->findnodes($tags_xpath)) 
    { 
      my $k = ""; 
      my $v = ""; 
      my $tag_type = substr ($tag->nodeName ,0,1);

      # print ("\t[$tag_type]: $tag\n"); #debug


      if ( $tag_type eq "t" ) # <tag>
      {
        $k = $tag->findnodes('./@k'); # key
        $v = $tag->findnodes('./@v'); # value
        $stmt_tag = qq(insert into $tag_table (id, key_attrib, val_attrib) values ( ?, ?, ? ); ); # end 
        my $rowCount = $dbh->do ($stmt_tag, undef, $way_id, $k, $v);              
        if ($rowCount < 0) 
        {
          print $DBI::errstr;
        }        
      }
      else # ( $tag_type eq "nd" ) # <nd> node reference
      {
        # my $start_ref_search_time = Time::HiRes::gettimeofday(); # debug fetch time
        my $ref = $tag->findnodes('./@ref'); # key

        # fetch $ref (node) information from database
        # my $stmt_fetch_node_by_id = qq(SELECT id, lat, lon FROM all_node_data where id = ? LIMIT 1;); # fetch only 1 row
        # my $sth_fetch_node_by_id = $dbh->prepare( $stmt_fetch_node_by_id );
        my $rv = $sth_fetch_node_by_id->execute($ref);

        if($rv < 0) {
          print "[$ref] $DBI::errstr";
        }

        while(my @row = $sth_fetch_node_by_id->fetchrow_array()) 
        {
          $match_ref_counter=$match_ref_counter+1;
          my $lat = $row[1]; # latitude
          my $lon = $row[2]; # longitude

          my $stmt = qq(insert into way_street_node_data (id, node_id, lat, lon) values ( ?, ?, ?, ? ); ); # 
          my $rowCount = $dbh->do ($stmt, undef, $way_id, $ref, $lat, $lon);              
          if($rowCount < 0) 
          {
            print $DBI::errstr;
          }              

        } # end loop over all fetched rows and storing them

        # my $stop_ref_search_time = Time::HiRes::gettimeofday(); # debug fetch time
        # printf("[%.2f]", $stop_ref_search_time - $start_ref_search_time);    # debug fetch time          


      
      } # end if sub element name is "n" meaning <nd> 
      
    } # end reading all sub nodes 
    
  } # end handle <way> element. We loop over all of them
  
} # end parse_way_nodes()

sub parse_node_nodes
{
  # the following function first stores the "sub elements" and at the end the "header".
  # main reason, we want less DML statements, and we want to store the "name" of the point with it (if there is one).
  # Once we find the attrib @k="name" we can then keep its value to write into the "header" table: node_data
  $seq=$seq+1;
  my $dom = $_[0];

  # my $test_dom = $_[0];
  # my $my_xpath = q{./tag[contains(@k,'name')]/..}; # default for <node> and <way>
  my $my_xpath = q{./*/..}; # default for <node> and <way>
  # if(my($dom) = $test_dom->findnodes($my_xpath)) 
  { 
    $nodes_conter=$nodes_conter+1;

    ## main dom node attributes to store
    my $parent_node_type = substr ($dom->nodeName, 0, 1)  ; # retrieve current node name
    my $id  = $dom->findnodes('./@id'); # latitude
    my $lat = $dom->findnodes('./@lat'); # latitude
    my $lon = $dom->findnodes('./@lon'); # longitude 

    ## sub elements
    my $table = "node_data";
    my $tag_table = "node_tag_data";  
    my $tags_xpath = q{./*}; # q{./tag}; #q{./tag[contains(@k,'name')]/..}; # work on nodes that have name attrib in their "<tag> sub element".

    my $name_value=""; # store the name of the <node> element if there is one
    my $stmt_main = qq( insert into $table     (id, lat, lon, name) values ( ?, ?, ?, ? ); ); # end qq. Create tag_data table                 
    my $stmt_tag  = qq( insert into $tag_table (id, key_attrib, val_attrib  ) values ( ?, ?, ? ); ); # end qq. Create tag_data table              

    # loop over all <node>s elements 
    if ($rebuild_node_tag_data eq "yes" || $task eq "all" )
    {
      foreach my $tag ($dom->findnodes($tags_xpath)) 
      {
        my $k = $tag->findnodes('./@k'); # key
        my $v = $tag->findnodes('./@v'); # value
        #$v =~ tr/"/'/; # replace \" with \'

        # store tag info
        my $rowCount = $dbh->do ($stmt_tag, undef, $id, $k, $v);              
        if($rowCount < 0) {
          print $DBI::errstr;
        }

        # handle main <elements> with <tag k="name" .. />
        if ( $k eq "name"  )
        {
          # print "found name\n";
          $name_value = $v;       
        } # name attribute

      }  # end fetch all sub elements      
    }    


    ### Store HEADER line (node_data) ###
    if ($rebuild_node_data eq "yes" || $task eq "all") ## do we have rebuild flag ?
    {
      my $rv = $dbh->do ($stmt_main, undef, $id, $lat, $lon, $name_value);
              
      if($rv < 0) {
        print "[$id]".$DBI::errstr."\n";        
      }    
    }



  }

 

} # end parse_node_nodes()

sub upload_all_node_nodes
{
  my $dom = $_[0];
  my $id  = $dom->findnodes('./@id'); # latitude
  my $lat = $dom->findnodes('./@lat'); # latitude
  my $lon = $dom->findnodes('./@lon'); # longitude 

  my $table = "all_node_data";
  my $stmt_main = qq( insert into $table     (id, lat, lon) values ( ?, ?, ? ); ); # end qq. Create tag_data table                 
  my $rv = $dbh->do ($stmt_main, undef, $id, $lat, $lon);
          
  if($rv < 0) {
    print $DBI::errstr;
    print "id: $id \n";
  } 


}

sub show_progress
{
    # show progress
    if ( $lines % 5000 == 0 )
    {
      if ( $lines % 50000 == 0 ) 
      {
        if ( $lines % 500000 == 0 ) 
        {
          print "|\n";
        }
        else 
        {
          print ",";
        }
      }
      else 
      {
         print ".";
      }

    } # end progress

    select()->flush();
}


# // read only nodes

# exit 0; # debug

if ($task eq "node" || $task eq "all" )
{
  my $reader = XML::LibXML::Reader->new(location => $filename)  # auto decoding on read
       or die "cannot read file.xml\n"; 

  print "\nDoing <node>, task: $task";
  select()->flush();
  while($reader->read) 
  {
    next unless $reader->nodeType == XML_READER_TYPE_ELEMENT;
    next unless ($reader->name eq 'node') ;

    my $dom = $reader->copyCurrentNode(1); # $dom = DOM this should return <node > element.
    # print "$dom\n";  #debug
    $lines = $lines + 1;

    show_progress();
    parse_node_nodes ($dom);

#    last unless $lines < 50000;

    $reader->next;
  } 

}
### end reading <node>

# read only way 
if ($task eq "way" || $task eq "all" )
{
  my $reader = XML::LibXML::Reader->new(location => $filename)  # auto decoding on read
       or die "cannot read file.xml\n"; 

  print "\nDoing <way>, task: $task";
  select()->flush();
  
  while($reader->read) 
  {
    next unless $reader->nodeType == XML_READER_TYPE_ELEMENT;
    next unless ($reader->name eq 'way') ;

    my $dom = $reader->copyCurrentNode(1); # $dom = DOM this should return <node > element.
    # print "$dom\n";  #debug
    $lines = $lines + 1;

    show_progress();
    parse_way_nodes ($dom);

#    last unless $lines < 50000;

    $reader->next;
  } 

}


# } # end full while

my $stop_time = Time::HiRes::gettimeofday();
printf("\n\nElapsed: %.2f\n===================\n", $stop_time - $start_time);

print "\nOverall: $seq tags read. \n"; # debug
print "Worked on: $lines node Lines \n"; # debug
print "Worked on <node>s: $nodes_conter \n"; # debug
print "Worked on <way>s: $way_counter \n"; # debug
print "\tref counter:: $ref_search_counter \n"; # debug
print "\tmatch_ref counter:: $match_ref_counter \n"; # debug

warn $DBI::errstr if $DBI::err;


$dbh->disconnect();




####### Main Body ######
#print "Parsing <tag>. \n===========\n";
# # parse_xpath ("/osm/node");
# # https://grantm.github.io/perl-libxml-by-example/xpath.html
# # //osm/node/tag[contains(@k,'name')]/parent::* // get the parent of a node <tag> that has an attribute "k" with the value "name"
# # //osm/node/tag[contains(@k,'name')]/..  # same as above but we use the "/.." to represnt the parent node.
# parse_xpath ("//osm/node/tag[contains(\@k,'name')]/.."); # match every element <node> that has a child node tag with attribute k and value name (k="name")


exit 0;

# my $rv = $dbh->do("$xxstmt");
# if($rv < 0) {
#   print $DBI::errstr;
# } 