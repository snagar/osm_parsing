select t1.id, t1.name 
from way_data t1, rel_tag_data t2
where t1
;

select t2.val_attrib
from rel_tag_data t2
where t2.tag = "m"
and t2.key_attrib = "way"
;

select * from node_data t3;

select * from node_tag_data t4
where key_attrib like '%wat%'
;

select distinct key_attrib from node_tag_data t4
order by 1
;

select t1.id, t1.name, t2.* 
from way_data t1, rel_tag_data t2
where t2.tag = "m"
and t2.key_attrib = "way"
and t1.id = t2.val_attrib
;

select t1.id, t1.name, t2.*, t3.key_attrib 
from way_data t1, rel_tag_data t2, node_tag_data t3
where t2.tag = "m"
and t2.key_attrib = "way"
and t1.id = t2.val_attrib
and t3.id = t1.id
--and t3.key_attrib = "name"
;

