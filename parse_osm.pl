#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

use XML::LibXML;

my $filename = $ARGV[0];
my $tag_name_to_parse = $ARGV[1]; ## to parse only one element instead the whole file. If empty then whole file will be parsed.

my $dom = XML::LibXML->load_xml(location => $filename);

sub parse_xpath
{
  my $xpath = $_[0];
  print "Parsing xpath: $xpath\n";
  
  # foreach my $node ($dom->findnodes('/MISSION/message_templates/message/mix')) {
  my $seq = 0;
  foreach my $node ($dom->findnodes($xpath)) { 
    $seq=$seq+1;	  
    my $node_name    = $node->nodeName; # retrieve current node name
    my $file_name = ""; 
    my $text = ""; 
    my $voice_lang_code = $node->findnodes('./@voice_lang_code'); # en-gb
    my $voice_name      = $node->findnodes('./@voice_name'); # "en-GB-Standard-A"
    my $voice_gender    = $node->findnodes('./@voice_gender'); # FEMALE / MALE
    my $voice_pitch     = $node->findnodes('./@voice_pitch'); # number between -20..+20. default should be 0.0. Added to original pitch
    my $voice_vol_gain  = $node->findnodes('./@voice_vol_gain'); # number -96.0 .. 16.0. Default 0.0
    my $voice_speak_rate = $node->findnodes('./@voice_speak_rate'); # number 0.25-4.0 default 1.0 or 0.0
   

    ### Prepare default values if attributes were not set.
    if ( $voice_lang_code eq "" )
     {
       $voice_lang_code = "en-gb";
     }

     if ( $voice_name eq "" )
     {
       $voice_name = "en-GB-Standard-A";
     }

     if ( $voice_gender eq "" )
     {
       $voice_gender = "FEMALE";
     }

     if ( $voice_speak_rate eq "" )
     {
       $voice_speak_rate = "0.0";
     }

     if ( $voice_pitch eq "" )
     {
       $voice_pitch = "0.0";
     }

     if ( $voice_vol_gain eq "" )
     {
       $voice_vol_gain = "0.0";
     }


    # Start per element parsing
    if ( $node_name eq "mix" )
    {
        my $type = $node->findnodes('./@track_type'); # read <mix track_type> attribute
        #print "type: $type, $text\n"; #debug
	      if ($type eq "text")
	      {
	       $file_name = $node->findnodes('../@name'); # read parent element name
         $text = $node->to_literal();
         
         if ( $tag_name_to_parse ne "" && $tag_name_to_parse ne $file_name ) # check name attribute
         {
           $text = ""; # this will cause the download to be skipped
           print "skipping ...\n";
         }
         
        }    
        else
        {
         $file_name = "";
        }
      }
      elsif( $node_name eq "dynamic_message" )
      {
        my $attrib1 = $node->findnodes('./@message_name_to_call'); # read <mix track_type> attribute
        $text = $node->to_literal();
        $file_name = $node->findnodes('./@name'); # read name attribute 
        print "Dynamic Node name is: $file_name \n"; # debug
        if ( $tag_name_to_parse ne "" && $tag_name_to_parse ne $file_name ) # check name attribute
        {
          $text = ""; # this will cause the download to be skipped
          print "skipping ...\n";
        }
	      elsif ($attrib1 ne "") # if attribute "message_name_to_call" is not empty, then we need to ignore any text that was defined in the node, since we dippend on a <message> element and that is taken care of already	      
	      {
	        $text = "";
	      }
	      elsif ($text ne "" )
	      {
	        if ( $file_name eq "" ) 
	        {
            $file_name=$node->findnodes('../@name');
	          $file_name="dynamic_message_for_${file_name}_$seq"; # construct name from flight leg name + sequence number
          }          
	      }


      }


      ## Start request and MP3 conversion
      #print "start request\n";
      $text =~ s/^\s+|\s+$//g;
      $text =~ s/["]//g; # remove any occurence of double quotes.
#### More example to special chars removal      
#      $text = `perl -pi -e 's/[+\-\@,]//g' $text`;
#      $text =~ s/["~!&*()\[\];.,:?^ `\\\/]+//g;


#### Example how to find/replace characters using arrays in perl
#      my @signs   =("\"","&","<",">");
#      my @htmlAmps=("&quot;", "&amp;", "&lt;", "&gt;");
#      my @signs   =("\"","<",">");
#      my @htmlAmps=("&quot;", "&lt;", "&gt;");
#      my $arr_size = scalar @signs;
#      
#      my $signCounter=0;
#      foreach my $sign (@signs) {
#        $text =~ s/[$sign]/$htmlAmps[$signCounter]/g;
#        $signCounter++;      
#      }
      

      if ( $text ne "" )
      {

        # prepare jason request template
        my $CTRLSource="request.tmpl";
        my $CTRLTarget="req_jason";       
        my $counter=1;
        my $next_counter=$counter;
          
        # `sed -e s/'%voice_lang_code%'/'${voice_lang_code}'/g    ${CTRLSource}>${CTRLTarget}.${counter}`; $next_counter++;
        # `sed -e "s|%text%|${text}|g"                           ${CTRLTarget}.${counter}>${CTRLTarget}.${next_counter}`; $counter=$next_counter; $next_counter++;

        `sed -e s/'%voice_lang_code%'/'${voice_lang_code}'/g    ${CTRLSource}>${CTRLTarget}.${counter}`; $next_counter++;
        `sed -e s/'%voice_name%'/'$voice_name'/g                ${CTRLTarget}.${counter}>${CTRLTarget}.${next_counter}`; $counter=$next_counter; $next_counter++; 
        `sed -e s/'%voice_gender%'/'$voice_gender'/g            ${CTRLTarget}.${counter}>${CTRLTarget}.${next_counter}`; $counter=$next_counter; $next_counter++;          
        `sed -e s/'%voice_pitch%'/'$voice_pitch'/g              ${CTRLTarget}.${counter}>${CTRLTarget}.${next_counter}`; $counter=$next_counter; $next_counter++;          
        `sed -e s/'%voice_speak_rate%'/'$voice_speak_rate'/g    ${CTRLTarget}.${counter}>${CTRLTarget}.${next_counter}`; $counter=$next_counter; $next_counter++;          
        `sed -e s/'%volume_gain_db%'/'$voice_vol_gain'/g        ${CTRLTarget}.${counter}>${CTRLTarget}.${next_counter}`; $counter=$next_counter; $next_counter++;          
        `sed -e "s|%text%|${text}|g"                            ${CTRLTarget}.${counter}>${CTRLTarget}.${next_counter}`; $counter=$next_counter; $next_counter++;
        
        # write to JSON file and cleanup
        `cp ${CTRLTarget}.${counter} request.json`; # prepare the final JSON file.
        `rm -f $CTRLTarget*`; # delete the temporary template files   
        # DEBUG  
        `cp request.json output/$file_name.request.json`; # DEBUG. Comment out once finish debug


        ## Call download_request.sh
        print "download: $text";	
	      my $DOWLOAD_FILE_NAME="file.base64.txt";
#	      `sh download_request.sh > $DOWLOAD_FILE_NAME`;
	      `sh download_request.sh > output/$file_name.base64.txt`;

        ## Convert and rename the downloaded file: from "synthesize-output-base64.txt" to something more meaningful
#        `/usr/bin/base64 $DOWLOAD_FILE_NAME -w 0 -di > output/$file_name.mp3`
        `/usr/bin/base64 output/$file_name.base64.txt -di > output/$file_name.mp3`
			  

      }

#      exit 0; # debug
    } # end for each element to search

}


####### Main Body ######
print "Parsing <mix>. \n===========\n";
parse_xpath ("/MISSION/message_templates/message/mix");
#exit 0; ## debug. 

print "Parsing <dynamic message>. \n==========\n";
parse_xpath ("/MISSION/flight_plan/leg/dynamic_message");
print "Parsing <message> in flight legs. \n==========\n";
parse_xpath ("/MISSION/flight_plan/message_templates/message/mix");

exit 0;




